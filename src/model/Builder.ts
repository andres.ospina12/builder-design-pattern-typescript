export interface Builder {
    produceDoor(): void;
    produceBath(): void;
    producePool(): void;
}