import { Builder } from "./model/Builder";

export class Director {
    private builder: Builder;

    public setBuilder(builder: Builder): void {
        this.builder = builder;
    }
    public buildMinimalViableHouse(): void {
        this.builder.produceDoor();
    }

    public buildFullFeaturedHouse(): void {
        this.builder.produceDoor();
        this.builder.produceBath();
        this.builder.producePool();
    }
}
