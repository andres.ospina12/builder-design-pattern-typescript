import { HouseBuilder } from "./HouseBuilder";
import { Director } from "./Director";

const clientCode = (director: Director) => {
    const builder = new HouseBuilder();
    director.setBuilder(builder);

    console.log('Standard basic product:');
    director.buildMinimalViableHouse();
    builder.getHouse().listParts();

    console.log('Standard full featured House:');
    director.buildFullFeaturedHouse();
    builder.getHouse().listParts();

    console.log('Custom House:');
    builder.produceDoor();
    builder.producePool();
    builder.getHouse().listParts();
}

const director = new Director();
clientCode(director);