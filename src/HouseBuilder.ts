import { House } from "./House";
import { Builder } from "./model/Builder";

export class HouseBuilder implements Builder {
    private house: House;
    constructor() {
        this.reset();
    }

    public reset(): void {
        this.house = new House();
    }

    public produceDoor(): void {
        this.house.parts.push('Door');
    }

    public produceBath(): void {
        this.house.parts.push('Bath');
    }

    public producePool(): void {
        this.house.parts.push('Pool');
    }
    public getHouse(): House {
        const result = this.house;
        this.reset();
        return result;
    }
}